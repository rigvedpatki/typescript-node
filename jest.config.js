module.exports = {
  moduleFileExtensions: ['ts', 'js', 'json'],
  clearMocks: true,
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  },
  rootDir: 'src',
  testMatch: ['<rootDir>/**/__tests__/*.ts'],
  setupTestFrameworkScriptFile: '<rootDir>/lib/utils/setup-tests.ts',
  testURL: 'http://localhost'
};
